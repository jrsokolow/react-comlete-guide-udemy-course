/**
 * Created by sokoloj1 on 2017-12-20.
 */

import React, {Component} from 'react';

import Person from './Person/Person'

class Persons extends Component {

  constructor(props) {
    super(props);
    console.log('[Persons.js] Inside Constructor', props);
  }

  componentWillMount() {
    console.log('[Persons.js] Inside component will mount');
  }

  componentDidMount() {
    console.log('[Persons.js] Inside component did mount');
  }

  componentWillReceiveProps(nextProps) {
    console.log('[UPDATE Persons.js] Inside componentWillReceiveProps', nextProps);
  }

  shouldComponentUpdate(nextProps, nextState) {
    console.log('[UPDATE Persons.js] Inside shouldComponentUpdate', nextProps, nextState);
    return true;
  }

  componentWillUpdate(nextProps, nextState) {
    console.log('[UPDATE Persons.js] Inside componentWillUpdate', nextProps, nextState);
  }

  componentDidUpdate(nextProps, nextState) {
    console.log('[UPDATE Persons.js] Inside componentDidUpdate', nextProps, nextState);
  }

  render() {
    console.log('[Persons.js] Inside render() ')
    return this.props.persons.map((person, index) => {
      return <Person name={person.name}
                     age={person.age}
                     click={() => this.props.clicked(index)}
                     key={person.id}
                     inputChangeHandler={(event) => this.props.changed(event, person.id)}/>;
    });
  }
}

export default Persons;
