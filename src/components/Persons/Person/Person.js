/**
 * Created by sokoloj1 on 2017-11-03.
 */

import React, {Component} from 'react';
import classes from './Person.css';

class Person extends Component {

  constructor(props) {
    super(props);
    console.log('[Person.js] Inside Constructor', props);
  }

  componentWillMount() {
    console.log('[Person.js] Inside component will mount');
  }

  componentDidMount() {
    console.log('[Person.js] Inside component did mount');
  }

  render() {
    console.log('[Person.js] Inside render()')
    return (
      <div className={classes.Person}>
        <p onClick={this.props.click}>I'm a {this.props.name}, I am {this.props.age} old!</p>
        <p>{this.props.children}</p>
        <input type="text" onChange={this.props.inputChangeHandler} value={this.props.name} />
      </div>
    )
  }
}

export default Person;
