import React, { Component } from 'react';
import classes from './App.css';
import Persons from '../components/Persons/Perons'
import '../components/Persons/Person/Person.css';
import Cockpit from '../components/Cockpit/Cockpit';

class App extends Component {

  constructor(props) {
    super(props);
    console.log('[App.js] Inside Constructor', props);
  }

  componentWillMount() {
    console.log('[App.js] Inside component will mount');
  }

  componentDidMount() {
    console.log('[App.js] Inside component did mount');
  }

  shouldComponentUpdate(nextProps, nextState) {
    console.log('[UPDATE App.js] Inside shouldComponentUpdate', nextProps, nextState);
    return true;
  }

  componentWillUpdate(nextProps, nextState) {
    console.log('[UPDATE App.js] Inside componentWillUpdate', nextProps, nextState);
  }

  componentDidUpdate(nextProps, nextState) {
    console.log('[UPDATE App.js] Inside componentDidUpdate', nextProps, nextState);
  }

  state = {
    persons:[
      {id:'aaa', name:'Jakub', age:34},
      {id:'bbb', name:'Ula', age:33},
      {id:'ccc', name:'Wojtek', age:7}
    ],
    showPersons:false
  };

  nameChangeHandler = (event, id) => {
    const personIndex = this.state.persons.findIndex(p => {
      return p.id === id;
    });

    const person = {...this.state.persons[personIndex]}; //copy of person
    person.name = event.target.value;

    const persons = [...this.state.persons];
    persons[personIndex] = person;

    this.setState({persons:persons});
  }

  deletePersonHandler = (personIndex) => {
    const persons = [...this.state.persons];
    persons.splice(personIndex, 1);
    this.setState({persons:persons});
  }

  togglePersonsHandler = () => {
    const doesShow = this.state.showPersons;
    this.setState({showPersons:!doesShow});
  }

  render() {

    console.log('[App.js] Inside render()');

    let persons = null;

    if(this.state.showPersons) {
      persons = <Persons persons={this.state.persons}
                   clicked={this.deletePersonHandler}
                   changed={this.nameChangeHandler} />
    }

    return (
        <div className={classes.App}>
          <Cockpit appTitle={this.props.title} showPersons={this.state.showPersons} persons={this.state.persons} clicked={this.togglePersonsHandler} />
          {persons}
        </div>
    );

  }
}

export default App;
